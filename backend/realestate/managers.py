from django.contrib.auth.models import BaseUserManager
from django.db import models

class MyUserManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        user = self.model(
            email = self.normalize_email(email),
            username = username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password=None):
        user = self.create_user(
            email=self.normalize_email(email),
            username = username,
            password = password,
        )
        user.is_admin=True
        user.is_staff=True
        user.is_superuser=True
        user.save(using=self._db)
        return user

    def update_user(self, instance, email, username, password=None):
        instance.email = self.normalize_email(email)
        instance.username = username
        instance.set_password(password)
        instance.save(using=self._db)
        return instance
# class MyProfileManager(models.Manager):
#     def create(self, **kwargs: Any) -> _T:
#         return super().create(**kwargs)