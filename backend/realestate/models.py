from email.policy import default
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from PIL import Image
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from backend.settings import MEDIA_ROOT
from django.db.models.signals import post_delete, pre_save, post_save
from django.dispatch import receiver
from realestate.managers import MyUserManager

@deconstructible
class UploadToPathAndRename(object):

    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # get filename
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            # set filename as random string
            filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.sub_path, filename)

class User(AbstractBaseUser):
    email = models.EmailField(verbose_name='email', max_length=60, unique=True, blank=False)
    username = models.CharField(max_length=30, unique=True, blank=False)
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    

    USERNAME_FIELD= 'email'
    REQUIRED_FIELDS = ['username',]

    objects = MyUserManager()

    def has_perm(self, perm, obj=None):
        return self.is_staff
    
    def __str__(self):
        return self.email

    def has_module_perms(self, app_label):
        return True

    class Meta:
        ordering = ['date_joined']

@receiver(post_save, sender=User)
def post_save_user(sender, instance, *args, **kwargs):
    try:
        UserFavorites.objects.create(user=instance)
    except:
        pass
    try:
        Profile.objects.create(user=instance)
    except:
        pass    

class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    image = models.ImageField(default=os.path.join(MEDIA_ROOT,'defaults','default-profile.jpg'), upload_to=UploadToPathAndRename(os.path.join(MEDIA_ROOT,'images','profiles')), blank=True)

    class Meta:
        ordering = ['user']

    def save(self, *args, **kwargs) -> None:
        super().save(*args, **kwargs)
        if Image.open(self.image.path):
            img = Image.open(self.image.path)

            if img.height > 300 or img.width > 300:
                output_size = (300, 300)
                img.thumbnail(output_size)
                img.save(self.image.path)

@receiver(post_delete, sender=Profile)
def post_delete_profile_image(sender, instance, *args, **kwargs):
    try:
        instance.image.delete(save=False)
    except:
        pass

@receiver(pre_save, sender=Profile)
def pre_save_profile_image(sender, instance, *args, **kwargs):
    try:
        old_img = instance.__class__.objects.get(id=instance.id).img.path
        try:
            new_img = instance.image.path
        except:
            new_img = None
        if new_img != old_img:
            import os
            if os.path.exists(old_img):
                os.remove(old_img)
    except:
        pass

class Listing(models.Model):
    owner = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='listings')
    title = models.CharField(max_length=30, blank=False, unique=True)
    price = models.IntegerField()
    description = models.TextField(blank=True)
    rooms = models.IntegerField()
    surface = models.IntegerField()
    pub_date = models.DateTimeField(auto_now_add=True)
    thumbnail = models.ImageField(upload_to=UploadToPathAndRename(os.path.join(MEDIA_ROOT,'images','listings','thumbnails')), blank=False)

    class Meta:
        ordering = ['pub_date']

    def save(self, *args, **kwargs) -> None:
        
        super().save(*args, **kwargs)
        img = Image.open(self.thumbnail.path)

        if img.height > 1080 or img.width > 1920:
            output_size = (1080, 1920)
            img.thumbnail(output_size)
            img.save(self.thumbnail.path)
    
@receiver(post_delete, sender=Listing)
def post_save_image(sender, instance, *args, **kwargs):
    try:
        instance.thumbnail.delete(save=False)
    except:
        pass

@receiver(pre_save, sender=Listing)
def pre_save_image(sender, instance, *args, **kwargs):
    try:
        old_img = instance.__class__.objects.get(id=instance.id).img.path
        try:
            new_img = instance.thumbnail.path
        except:
            new_img = None
        if new_img != old_img:
            import os
            if os.path.exists(old_img):
                os.remove(old_img)
    except:
        pass

class ListingImage(models.Model):
    listing = models.ForeignKey(Listing, related_name='images', on_delete=models.CASCADE)
    # detail = models.CharField(blank=False, max_length=30)
    image = models.ImageField(upload_to=UploadToPathAndRename(os.path.join(MEDIA_ROOT,'images','listings')), blank=False)
    created = models.DateTimeField(auto_now_add=True)
    isThumbnail = models.BooleanField(default=False)

    class Meta:
        ordering = ['created']

    def save(self, *args, **kwargs) -> None:
        
        super().save(*args, **kwargs)
        img = Image.open(self.image.path)

        if img.height > 1080 or img.width > 1920:
            output_size = (1080, 1920)
            img.thumbnail(output_size)
            img.save(self.image.path)
    
@receiver(post_delete, sender=ListingImage)
def post_save_image(sender, instance, *args, **kwargs):
    try:
        instance.image.delete(save=False)
    except:
        pass

@receiver(pre_save, sender=ListingImage)
def pre_save_image(sender, instance, *args, **kwargs):
    try:
        old_img = instance.__class__.objects.get(id=instance.id).img.path
        try:
            new_img = instance.image.path
        except:
            new_img = None
        if new_img != old_img:
            import os
            if os.path.exists(old_img):
                os.remove(old_img)
    except:
        pass

class UserFavorites(models.Model):
    user = models.OneToOneField(User, related_name='favorites', on_delete=models.CASCADE)
    listings = models.ManyToManyField(Listing, related_name='listings', blank=True)

    