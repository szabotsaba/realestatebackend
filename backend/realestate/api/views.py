from rest_framework import viewsets, permissions
from .permissions import IsImageOwnerOrReadOnly, IsOwnerOrReadOnly, IsAdminOrReadOnly, IsUser
from realestate.models import Profile
from .serializers import ListingImageSerializer, ListingSerializer, ProfileSerializer, UserSerializer
from realestate.models import User, Listing, ListingImage
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

@api_view(['POST', 'DELETE'])
def AddOrRemoveFavorite(request):
    if request.user.is_authenticated:
        if Listing.objects.get(id=request.data.get('listing_id')):
            if request.method == 'POST':
                request.user.userfavorites.favorites.add(Listing.objects.get(id=request.data.get('listing_id')))
                return Response(data='added', status=status.HTTP_202_ACCEPTED)
            if request.method == 'DELETE':
                request.user.userfavorites.favorites.remove(Listing.objects.get(id=request.data.get('listing_id')))
                return Response(data='deleted', status=status.HTTP_202_ACCEPTED)
        return Response(data='No such listing', status=status.HTTP_400_BAD_REQUEST, template_name='Unavailable', headers=None, content_type=None)
    return Response(data='You are not authenticated', status=status.HTTP_401_UNAUTHORIZED, template_name='Unauthorized', headers=None, content_type=None)


class ListingImageViewSet(viewsets.ModelViewSet):
    queryset = ListingImage.objects.all()
    serializer_class = ListingImageSerializer
    permission_classes = [IsAdminOrReadOnly | IsImageOwnerOrReadOnly]

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAdminUser | IsUser]

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permissino_classes = [permissions.AllowAny]

class ListingViewSet(viewsets.ModelViewSet):
    queryset = Listing.objects.all()
    serializer_class = ListingSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                        IsOwnerOrReadOnly | IsAdminOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=Profile.objects.get(user=self.request.user))

    # def get_queryset(self):
    #     user = self.request.user
    #     return Listing.objects.filter(user=user)