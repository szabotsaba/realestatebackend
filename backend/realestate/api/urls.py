from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

router = DefaultRouter()
router.register(r'users', viewset=views.UserViewSet)
router.register(r'listings', viewset=views.ListingViewSet)
router.register(r'profiles', viewset=views.ProfileViewSet)
router.register(r'listing-images', viewset=views.ListingImageViewSet)

urlpatterns =[
    path('', include(router.urls)),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('favorites/', views.AddOrRemoveFavorite, name='AddOrRemoveFavorite'),
]