from realestate.models import User, Listing, Profile, UserFavorites, ListingImage
from rest_framework import serializers
from drf_extra_fields.fields import Base64ImageField

class ListingImageSerializer(serializers.HyperlinkedModelSerializer):
    listing = serializers.ReadOnlyField(source='listing.title')
    image = Base64ImageField()
    class Meta:
        model = ListingImage
        fields = '__all__'

class ListingSerializer(serializers.HyperlinkedModelSerializer):
    # images = serializers.HyperlinkedRelatedField(many=True, view_name='listingimages-detail', read_only= False, queryset=ListingImage.objects.all())
    # images = serializers.HyperlinkedRelatedField(view_name='listingimages-detail', read_only=True)
    thumbnail = Base64ImageField()
    images = ListingImageSerializer(many=True)
    owner = serializers.ReadOnlyField(source="owner.user.username")
    id = serializers.IntegerField(read_only=True)
    class Meta:
        model = Listing
        fields = '__all__'

    def create(self, validated_data):
        images_data = validated_data.pop('images')
        listing = Listing.objects.create(**validated_data)
        for image_data in images_data:
            ListingImage.objects.create(listing=listing, **image_data)
        return listing

    def update(self, instance, validated_data):
        images_data = validated_data.pop('images')
        listing = Listing.objects.update(instance=instance, **validated_data)
        for image_data in images_data:
            ListingImage.objects.create(listing=listing, **image_data)
        return listing

class UserFavoritesSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.id')
    favorites = ListingSerializer(many=True, read_only=True)
    class Meta:
        model = UserFavorites
        fields = '__all__'
        
class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    user = serializers.ReadOnlyField(source='user.username')
    listings = serializers.HyperlinkedRelatedField(many=True, view_name='listing-detail', read_only=True)

    class Meta:
        model = Profile
        fields = '__all__'

class UserSerializer(serializers.HyperlinkedModelSerializer):
    userfavorites = UserFavoritesSerializer(many=False, read_only=True)
    profile = serializers.HyperlinkedRelatedField(many=False, view_name='profile-detail', read_only=True)
    is_active = serializers.ReadOnlyField()
    is_admin = serializers.ReadOnlyField()
    is_staff = serializers.ReadOnlyField()
    is_superuser = serializers.ReadOnlyField()
    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        username = validated_data.pop('username')
        password = validated_data.pop('password')
        email = validated_data.pop('email')
        user = User.objects.create_user(username=username, password=password, email=email)
        return user

    def update(self, instance, validated_data):
        username = validated_data.pop('username')
        password = validated_data.pop('password')
        email = validated_data.pop('email')
        user = User.objects.update_user(instance=instance, email=email, username=username, password=password)
        return user
